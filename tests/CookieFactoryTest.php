<?php

declare(strict_types=1);

namespace Tests;

use Nolikein\Cookie\CookieFactory;
use PHPUnit\Framework\TestCase;

class CookieFactoryTest extends TestCase
{
    public function testCreation()
    {
        $factory = new CookieFactory();
        $cookieEmpty = $factory->createCookie('name');
        $cookieFilled = $factory->createCookie('name', 'value', time() + 3600, '/my/path', 'www.domain.org', true, true);

        // Test default values
        $this->assertEquals('name', $cookieEmpty->getName());
        $this->assertEquals('', $cookieEmpty->getValue());
        $this->assertNull($cookieEmpty->getExpirationTime());
        $this->assertEquals('', $cookieEmpty->getPath());
        $this->assertEquals('', $cookieEmpty->getDomain());
        $this->assertFalse($cookieEmpty->isSecure());
        $this->assertFalse($cookieEmpty->isHttpOnly());
        $this->assertEquals('Lax', $cookieEmpty->getSameSite());

        // Test filled values
        $this->assertEquals('name', $cookieFilled->getName());
        $this->assertEquals('value', $cookieFilled->getValue());
        $this->assertInstanceOf(\DateTime::class, $cookieFilled->getExpirationTime());
        $this->assertEquals('/my/path', $cookieFilled->getPath());
        $this->assertEquals('www.domain.org', $cookieFilled->getDomain());
        $this->assertTrue($cookieFilled->isSecure());
        $this->assertTrue($cookieFilled->isHttpOnly());
        $this->assertEquals('Lax', $cookieFilled->getSameSite());
    }
}