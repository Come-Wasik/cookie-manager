<?php

declare(strict_types=1);

namespace Tests;

use Nolikein\Cookie\Cookie;
use Nolikein\Cookie\InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class CookieTest extends TestCase
{
    public function testCorrectWithAndgetCall()
    {
        $cookie = new Cookie();

        // Gets default values
        $this->assertEquals('', $cookie->getName());
        $this->assertEquals('', $cookie->getValue());
        $this->assertNull($cookie->getExpirationTime());
        $this->assertFalse($cookie->isExpired());
        $this->assertEquals('', $cookie->getPath());
        $this->assertEquals('', $cookie->getDomain());
        $this->assertFalse($cookie->isSecure());
        $this->assertFalse($cookie->isHttpOnly());
        $this->assertEquals('Lax', $cookie->getSameSite());

        // Value affectation
        $cookie = $cookie
            ->withName('essai')
            ->withValue('value')
            // 2 possible types
            ->withExpirationTime(time() + 3600)
            ->withExpirationTime((new \DateTime('now'))->add(new \DateInterval('P1D')))
            ->withPath('/my/path')
            ->withDomain('www.domain.org')
            ->withSecurity(true, true)
            // Enumeration
            ->withSameSite(Cookie::SAMESITE_STRICT)
            ->withSameSite(Cookie::SAMESITE_LAX)
            ->withSameSite(Cookie::SAMESITE_NONE)
            // Case do not change
            ->withSameSite('None')
            ->withSameSite('none');

        // Verification
        $this->assertEquals('essai', $cookie->getName());
        $this->assertEquals('value', $cookie->getValue());
        $this->assertInstanceOf(\DateTime::class, $cookie->getExpirationTime());
        $this->assertFalse($cookie->isExpired());
        $this->assertTrue($cookie->withExpirationTime(time() - 3600)->isExpired());
        $this->assertEquals('/my/path', $cookie->getPath());
        $this->assertEquals('www.domain.org', $cookie->getDomain());
        $this->assertTrue($cookie->isSecure());
        $this->assertTrue($cookie->isHttpOnly());
        $this->assertEquals(Cookie::SAMESITE_NONE, $cookie->getSameSite());
    }

    public function testSamesiteEnumeration()
    {
        $cookie = new Cookie();

        foreach (Cookie::SAMESITE_POSSIBLE_VALUES as $value) {
            $cookie = $cookie
            ->withSameSite($value);
        }
        $this->assertTrue(true);
    }

    public function testExpirationTimeTypeError()
    {
        $this->expectException(InvalidArgumentException::class);
        $cookie = new Cookie();
        $cookie = $cookie
            ->withExpirationTime('throw error');
    }

    public function testPathError()
    {
        $this->expectException(InvalidArgumentException::class);
        $cookie = new Cookie();
        $cookie = $cookie
            ->withPath('£ is an illegal character');
    }

    public function testDomainError()
    {
        $this->expectException(InvalidArgumentException::class);
        $cookie = new Cookie();
        $cookie = $cookie
            ->withDomain('This is an incorrect domain name');
    }
}