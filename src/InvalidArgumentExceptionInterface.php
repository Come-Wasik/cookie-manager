<?php

namespace Nolikein\Cookie;

/**
 * This interface can be use in catch statement to catch
 * only exceptions which implements this interface.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
interface InvalidArgumentExceptionInterface
{

}