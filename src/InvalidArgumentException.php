<?php

namespace Nolikein\Cookie;

/**
 * This exception can be used to catch more accurately
 * an invalid argument exception thrown from the Cookie object.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class InvalidArgumentException extends \InvalidArgumentException
{
    
}