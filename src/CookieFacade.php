<?php

namespace Nolikein\Cookie;

/**
 * CookieFacade is a Facade for an object implementing the CookieInterface.
 * It allows to set or unset a cookie in the headers.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class CookieFacade
{
    public function setCookie(CookieInterface $cookie): bool
    {
        return setcookie($cookie->getName(), $cookie->getValue(), [
            'expires' => $cookie->getExpirationTime()->getTimestamp(),
            'path' => $cookie->getPath(),
            'domain' => $cookie->getDomain(),
            'secure' => $cookie->isSecure(),
            'httponly' => $cookie->isHttpOnly(),
            'samesite' => $cookie->getSameSite()
        ]);
    }

    public function unsetCookie(CookieInterface $cookie)
    {
        return setcookie($cookie->getName(), '', time() - 3600);
    }
}