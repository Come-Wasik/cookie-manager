<?php

namespace Nolikein\Cookie;

use Nolikein\Cookie\InvalidArgumentException;

/**
 * Cookie is a class which represent a web browser cookie.
 * It implements the cookieInterface.
 * 
 * This class cannot set or unset a cookie itself. For
 * that, use the CookieFacade or use the methods of this
 * object.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class Cookie implements CookieInterface
{
    const SAMESITE_NONE = 'None';
    const SAMESITE_LAX = 'Lax';
    const SAMESITE_STRICT = 'Strict';

    const SAMESITE_POSSIBLE_VALUES = [
        self::SAMESITE_NONE,
        self::SAMESITE_LAX,
        self::SAMESITE_STRICT
    ];

    /** @var string $name The cookie name */
    protected $name = '';

    /** @var string $value The cookie value */
    protected $value = '';

    /** @var \Datetime $expirationTime The time when the cookie is expired */
    protected $expirationTime = null;

    /** @var string $path The cookie path */
    protected $path = '';

    /** @var string $domain The domain where the cookie is usable */
    protected $domain = '';

    /** @var bool $isSecure Tell if the cookie will be given only at https urls */
    protected $isSecure = false;

    /** @var bool $isHttpOnly Tell if the cookie won't be accesible on script langages */
    protected $isHttpOnly = false;

    /** @var string $sameSite Tell if the cookie MUST be used only at the same site */
    protected $sameSite = 'Lax';

    public function withName(string $name): CookieInterface
    {
        if(empty($name)) {
            throw new InvalidArgumentException('The name of the cookie cannot be empty', 500);
        }

        $copy = clone $this;
        $copy->name = trim($name);
        return $copy;
    }

    public function withValue(string $value): CookieInterface
    {
        $copy = clone $this;
        $copy->value = $value;
        return $copy;
    }

    public function withExpirationTime($expirationTime): CookieInterface
    {
        if(!is_int($expirationTime) && !($expirationTime instanceof \DateTime)) {
            throw new InvalidArgumentException('The expiration time MUST be an int or a \Datetime object', 500);
        }

        if(is_int($expirationTime)) {
            if($expirationTime === 0) {
                $expirationTime = null;
            } else {
                $timestamp = $expirationTime;
                $expirationTime = (new \DateTime())->setTimestamp($timestamp);    
            }
        }

        $copy = clone $this;
        $copy->expirationTime = $expirationTime;
        return $copy;
    }

    public function withPath(string $path): CookieInterface
    {
        if(filter_var($path, FILTER_SANITIZE_URL) !== $path) {
            throw new InvalidArgumentException('The path is invalid', 500);
        }

        // Clean path
        if(!empty($path)) {
            $path = '/'.(ltrim($path, '/'));
        }

        $copy = clone $this;
        $copy->path = $path;
        return $copy;
    }

    public function withDomain(string $domain): CookieInterface
    {
        if(!empty($domain) && !filter_var($domain, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME)) {
            throw new InvalidArgumentException('The domain is invalid', 500);
        }

        $copy = clone $this;
        $copy->domain = $domain;
        return $copy;
    }

    public function withSecurity(bool $isSecure, bool $isHttpOnly = false): CookieInterface
    {
        $copy = clone $this;
        $copy->isSecure = $isSecure;
        $copy->isHttpOnly = $isHttpOnly;
        return $copy;
    }

    public function withSameSite(string $sameSite): CookieInterface
    {
        $sameSite = ucfirst($sameSite);

        if(!in_array($sameSite, self::SAMESITE_POSSIBLE_VALUES)) {
            throw new InvalidArgumentException('The '.$sameSite.' value is unknown. You can use: '.implode(', ', self::SAMESITE_POSSIBLE_VALUES), 500);
        }

        $copy = clone $this;
        $copy->sameSite = $sameSite;
        return $copy;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getExpirationTime(): ?\DateTime
    {
        return $this->expirationTime;
    }

    public function isExpired(): bool
    {
        if(is_null($this->expirationTime)) {
            return false;
        }
        return ($this->expirationTime < (new \DateTime('now')) ? true : false);
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function isSecure(): bool
    {
        return $this->isSecure;
    }

    public function isHttpOnly(): bool
    {
        return $this->isHttpOnly;
    }

    public function getSameSite(): string
    {
        return $this->sameSite;
    }
}