<?php

namespace Nolikein\Cookie;

/**
 * CookieFactory is a factory to create a Cookie object. This last
 * implements the cookieInterface.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */

class CookieFactory implements CookieFactoryInterface
{
    public function createCookie(string $name, string $value = "", $expire = 0, string $path = "", string $domain = "", bool $isSecure = false, bool $isHttpOnly = false): CookieInterface
    {
        return (new Cookie())
            ->withName($name)
            ->withValue($value)
            ->withExpirationTime($expire)
            ->withPath($path)
            ->withDomain($domain)
            ->withSecurity($isSecure, $isHttpOnly);
    }
}